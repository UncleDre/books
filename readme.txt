This app is for personal books saving.
using NodeJS, Express, MongoDB, Mongoose, Bootstrap

there is a table 4x2x3(hardcoded) with 3 pages of books
each book has detailed info, there is a btn in table for it

User can add edit or remove books
description of book is saving in Db

to see how it works on real server move here: https://fathomless-refuge-42752.herokuapp.com/bookslist/

to run on local server in Windows

1. Install Node.js, MongoDb
2. Start MongoDb server
3. From project folder start node server
4. open http://localhost:3000 in browser