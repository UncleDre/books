var mongoose = require("mongoose");
var Book = mongoose.model("Book");

var send_json_response = function(res, status, content){
    res.status(status); 
    res.json(content);
};

var create_update_obj = function(req){
    var update_obj = {};
    if (req.body.author) 
        update_obj.author = req.body.author; 
    if (req.body.title)    
        update_obj.title = req.body.title;
    if (req.body.rating)    
        update_obj.rating = req.body.rating;
    if (req.body.review)    
        update_obj.review = req.body.review; 
    if (req.body.status)   
        update_obj.status = (req.body.status === "done");
    if (req.body.details){
        update_obj.details = {};
        if (req.body.details.file_path)
            update_obj.details.file_path = req.body.details.file_path;
        if (req.body.details.cover_src)
            update_obj.details.cover_src = req.body.details.cover_src;
    }    
    return update_obj;
};

var check_storage_place = function(){
    Book.count({}, function(err, count){
        if(err)
            send_json_response(res, 400, err);
        if(count > 23){
            Book
                .findOneAndRemove({})
                .exec(function(err, doc){
                    if(err)
                        send_json_response(res, 400, err);
                    console.log("will delete book : ", doc.title);
                });
        }
    });
};

module.exports.get_booksList = function(req, res) {
    Book
        .find()
        .exec(function(err, books) {
            if (!books) { 
                send_json_response(res, 404, { 
                "message": "locationid not found" 
                }); 
                return;
            } else if (err) { 
                send_json_response(res, 404, err); 
                return; 
            }
            send_json_response(res, 200, books);
            });
     
};

module.exports.add_book = function(req, res) {
    if (!req.body){
        send_json_response(res, 404, { 
                "message": "Req body not found" 
                });
        return;
        }
    //check if there are place in collection if not delete one doc
    check_storage_place();
    
    var book = {
        author:         req.body.author || "no name",
        title:          req.body.title || "no title",
        rating:         req.body.rating,
        review:         req.body.review,
        status:         (req.body.status === "done"),
        details:    {
                        file_path:      req.body.details.file_path || "/books/Getting_MEAN.pdf",
                        cover_src:      req.body.details.cover_src || "/images/book_covers/cat.jpg"
                    }
    };
    Book.create(book, function(err, new_book) {
        if (err) {
            send_json_response(res, 400, err);
        } else {
            send_json_response(res, 201, new_book);
            }
        });
     
};

module.exports.delete_book = function(req, res) {
    if (req.params && req.params.book_id){  
    Book
        .findByIdAndRemove(req.params.book_id)
        .exec(function(err, book){
            if (err){
                send_json_response(res, 404, err); 
                return;
            }
            if (!book){
                send_json_response(res, 404, {"message": "no book with such id"}); 
                return;
            }
            else
                send_json_response(res, 200, {"message": "Book deleted"});          
        });
    }
    else
        send_json_response(res, 404, {"message": "No book id"});
};

module.exports.update_book = function(req, res) {
    var update_obj = create_update_obj(req);
    //console.log("create_update_obj return = ", update_obj);
    if (req.params && req.params.book_id){  
    Book
        .findById(req.params.book_id)
        .exec(function(err, book){
            if (err){
                send_json_response(res, 404, err); 
                return;
            }
            if (!book){
                send_json_response(res, 404, {"message": "no book with such id"}); 
                return;
            }
            for (parameter in update_obj)
                {
                    console.log("parameter = ", parameter);
                    if (parameter === "details"){
                        book.details.file_path = update_obj[parameter].file_path;
                        book.details.cover_src = update_obj[parameter].cover_src;
                    }
                    else
                        book[parameter] = update_obj[parameter];
                }
            book.save(function(err, book){
                    if (err) 
                        send_json_response(res, 404, err);
                    else
                        send_json_response(res, 200, {"message": "book updated"});
                });                       
        });
    }
    else
        send_json_response(res, 404, {"message": "No book id in request params"});
};

module.exports.book_details = function(req, res) {
    if (req.params && req.params.book_id){  
    Book
        .findById(req.params.book_id)
        .exec(function(err, book){
            if (err){
                send_json_response(res, 404, err); 
                return;
            }
            if (!book){
                send_json_response(res, 404, {"message": "no book with such id"}); 
                return;
            }
            else
                send_json_response(res, 200, book);          
        });
    }
    else
        send_json_response(res, 404, {"message": "No book id"});
};


