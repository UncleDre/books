var express = require('express');
var router = express.Router();
var bookslistCtrl = require('../controllers/API_bookslist'); 

//books
router.get('/bookslist',                    bookslistCtrl.get_booksList);       //done
//get book details
router.get('/bookslist/:book_id',           bookslistCtrl.book_details);        //done
router.get('/bookslist/edit/:book_id',      bookslistCtrl.book_details);        //done
//add new book in db
router.post('/bookslist/new',               bookslistCtrl.add_book);            //done
//update book in db
router.put('/bookslist/edit/:book_id',      bookslistCtrl.update_book);         //done
//delete book
router.delete('/bookslist/:book_id',        bookslistCtrl.delete_book);         //done

module.exports = router;

