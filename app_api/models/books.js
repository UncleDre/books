var mongoose = require( 'mongoose' );

var book_details_schema = new mongoose.Schema({
    file_path:      {type: String, required: true},
    cover_src:      String
});

var book_schema = new mongoose.Schema({
    author:         {type: String, required: true},
    title:          {type: String, required: true},
    rating:         {type: Number, "default": 0, min: 0, max: 5},
    review:         String,
    status:         Boolean,
    add_date:       {type: Date, "default": Date.now},
    details:        book_details_schema
});

mongoose.model('Book', book_schema, 'books_list');


