var express = require('express');
var router = express.Router();
var bookslistCtrl = require("../controllers/bookslist");

/* GET bookslist page. */
router.get('/',                             bookslistCtrl.books_list_page);
router.get('/bookslist',                    bookslistCtrl.books_list_page);
router.get('/bookslist/details/:book_id',   bookslistCtrl.book_details);

/*GET add book page. */
router.get('/bookslist/new',                bookslistCtrl.add_new_book);
router.post('/bookslist/new',               bookslistCtrl.do_add_book);

//  edit book page
router.get('/bookslist/edit/:book_id',      bookslistCtrl.edit_book);
router.post('/bookslist/edit/:book_id',     bookslistCtrl.do_edit_book);

//DELETE  book page
router.get('/bookslist/delete/:book_id',    bookslistCtrl.delete_book);

module.exports = router;
