var request = require('request');

var api_options = { 
    server : "http://127.0.0.1:3000" 
    }; 
if (process.env.NODE_ENV === 'production') {
    api_options.server = "https://fathomless-refuge-42752.herokuapp.com";
}

var image_names = ["moon", "blue_moon", "cat", "house", "details"]; //while upload doesnt work

var _showError = function(req, res, statusCode){
    var title, text;
    if (statusCode === 404) { 
        title = "404, page not found"; 
        text = ["Page not found. Sorry."]; 
    } else { 
        title = "" + statusCode + ", something's gone wrong"; 
        text = ["Something, somewhere, has gone wrong."]; 
    }
    res.status(statusCode);
    res.render('generic-text', { 
        title : title, 
        text : text 
    }); 
};

var render_booklist =       function(req, res, response_body){
    var message; 
    if (!(response_body instanceof Array)) { 
        message = "API lookup error"; 
        response_body = []; 
    } else { 
        if (!response_body.length) { 
            message = "No books on shelf"; 
        } 
    }
     res.render('books', 
            { title: 'Book`s shelf',
            active_page:    "Books_nav_ref",
            tableHeaders: {
                num:            "№",
                author:         "Author",
                title:          "Title",
                rating:         "Book rating",
                status:         "Status",
                actions:        "Actions"
            },
            books: response_body,
            table: {
                pages:  3,
                rows:   4
            },
            message: message
    });
};

var render_book_details =   function(req, res, response_body){
    var message; 
    if (!response_body) { 
        message = "API lookup error"; 
    }
    res.render("book_details", {
        title:              "Book`s details",
        active_page:        "Details_nav_ref",
        message:            message,
        choosedBook: {
            author:         response_body.author,
            title:          response_body.title,
            rating:         response_body.rating,
            review:         response_body.review,
            cover_src:      response_body.details.cover_src
        }  
    });
};

var render_book_edit_form = function(req, res, response_body){
    var title = "Edit Book";
    var active_page = "Edit_nav_ref";
    res.render("edit_book", {
        active_page:    active_page,
        title:          title,
        statuses:       ["done", "not read"],
        book:           response_body,
        images:         image_names,
        err:            req.query.err
    });
};

var render_add_book_form =  function(req, res, response_body){
    var title = "Add Book";
    var active_page = "Add_nav_ref";
    res.render("add_book", {
        active_page:    active_page,
        title:          title,
        statuses:       ["done", "not read"],
        images:         image_names,
        err:            req.query.err
    });
};


module.exports.books_list_page =    function(req, res, next) {
  var request_options, path;
    path = "/api/bookslist";
    request_options = {
        url:        api_options.server + path,
        method:     "GET",
        json:       {},
        qs:         {}
    };
    request(request_options, function(err, response, body) {
        if (response.statusCode === 200)
            render_booklist(req, res, body);
        else 
            _showError(req, res, response.statusCode);
    });
};

module.exports.book_details =       function(req, res, next){
    var request_options, path, render_callback;
    render_callback = (req.query.edit_page) ? render_book_edit : render_book_details;
    path = "/api/bookslist/" + req.params.book_id;
    request_options = {
        url:        api_options.server + path,
        method:     "GET",
        json:       {},
        qs:         {}
    };
    request(request_options, function(err, response, body) {
        if (response.statusCode === 200)
            render_callback(req, res, body);
        else 
            _showError(req, res, response.statusCode);
    });
};

module.exports.add_new_book =       function(req, res, next){
    render_add_book_form(req, res, null);
};

module.exports.do_add_book =        function(req, res, next){
    //if not all required fields is filled
    if (!req.body.author || !req.body.title){
        res.redirect('/bookslist/new?err=val');
        return;
    } 
    
    var request_options;
    var path = "/api/bookslist/new";
    var cover_src = "/images/book_covers/"+req.body.cover_name+".jpg";
    
    request_options = {
        url:    api_options.server + path,
        method: "POST",
        json:   {
                author:         req.body.author,
                title:          req.body.title,
                rating:         parseInt(req.body.rating, 10),
                review:         req.body.review,
                status:         req.body.status,
                details:        {
                                file_path:      "/books/Getting_MEAN.pdf",
                                cover_src:      cover_src
                                }
                },
        qs:{}
    };
    
    request(request_options, function(err, response, body) {
        if(response.statusCode === 201)
            res.redirect(/bookslist/);
        else
            _showError(req, res, response.statusCode);
    });
};

module.exports.edit_book =          function(req, res, next){
    var request_options, path;
    path = "/api/bookslist/edit/" + req.params.book_id;
    request_options = {
        url:        api_options.server + path,
        method:     "GET",
        json:   {},
        qs:     {}
    };
    request(request_options, function(err, response, body) {
        if(response.statusCode === 200)
            render_book_edit_form(req, res, body);
        else
            _showError(req, res, response.statusCode);   
    });
};

module.exports.do_edit_book =       function(req, res, next){
    var path = "/api/bookslist/edit/"+req.params.book_id; 
    var request_options = {
        url:        api_options.server + path,
        method:     "PUT",
        json:   {
                author:         req.body.author,
                title:          req.body.title,
                rating:         parseInt(req.body.rating, 10),
                review:         req.body.review,
                status:         req.body.status,
                add_date:       req.body.add_date,
                details:        {
                                file_path:      req.body.file_path || "/books/Getting_MEAN.pdf",
                                cover_src:      "/images/book_covers/"+req.body.cover_name+".jpg"
                                }                
                },
        qs:     {}
    };
    
    console.log("edit request_options json = ", request_options.json);
    
    request(request_options, function(err, response, body) {
        if(response.statusCode === 200)
            res.redirect(/bookslist/);
        else
            _showError(req, res, response.statusCode);
    });
};

module.exports.delete_book =        function(req, res, next){
    var path = "/api/bookslist/" + req.params.book_id;
    var request_options = {
        url:        api_options.server + path,
        method:     "DELETE",
        json:       {},
        qs:         {}
    };
    
    request(request_options, function(err, response, body) {
        if(response.statusCode === 200)
            res.redirect(/bookslist/);
        else
            _showError(req, res, response.statusCode);
    });
};

