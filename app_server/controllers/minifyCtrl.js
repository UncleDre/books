var uglifyJs = require("uglify-js");
var fs = require('fs');

var appClientFiles = [ 
    'public/javascripts/bookslist.js',
    'public/javascripts/jquery-3.2.1.js',
    'public/javascripts/validation.js'
];

//    

var create_from_files = [];

for(var i=0; i < appClientFiles.length; i++){
    create_from_files.push(fs.readFileSync(appClientFiles[i], 'utf8'));
}

var uglified = uglifyJs.minify(create_from_files, { compress : false });

fs.writeFile('public/javascripts/books.min.js', uglified.code, function (err){
    if(err) { 
        console.log(err); 
    } else { 
        console.log('Script generated and saved: books.min.js'); 
    } 
});
