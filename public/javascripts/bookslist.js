var update_btn_color = function(obj){
    var all_btns = document.getElementsByName("page_btn");   
    for(var i=0; i < all_btns.length; i++)
    {
        if(all_btns[i].id === obj.id)
        {
            all_btns[i].classList.add("pressed");
            continue;
        }
        all_btns[i].classList.remove("pressed");
    }
};

var switch_table_page = function(obj){
    var all_pages = document.getElementsByName("page");   
    for(var i=0; i < all_pages.length; i++)
    {
        if(all_pages[i].id === obj.id)
        {
            all_pages[i].classList.remove("hidden");
            all_pages[i].classList.add("visible");
            continue;
        }
        all_pages[i].classList.add("hidden");
    }
    update_btn_color(obj);    
    window.scroll(0, 0);
};

var btn_change_status = function(obj){
    var pressed = false;
    for(var i=0; i < obj.classList.length; i++)
    {
        if (obj.classList[i] === "pressed")
            pressed = true;
    }
    if (!pressed)    {
        obj.classList.add("pressed");
        obj.innerHTML = "on";
        console.log("become pressed : "+ obj.classList);
    }
    else {
        obj.classList.remove("pressed");
        obj.innerHTML = "off"; 
        console.log("not pressed : "+ obj.classList);
    }
};

var nav_btn_change_visibility = function(id){  
    var obj = document.getElementById(id);
    console.log("btn_change_visibility : ", obj.classList);
    for(var i=0; i < obj.classList.length; i++)
    {
        if (obj.classList[i] === "collapse"){
            //obj.classList.remove("collapse");
            //obj.classList.add("visible");
            obj.classList.add("in");
            console.log("btn_change_visibility : ", obj.classList);
            return;
        }
        if (obj.classList[i] === "visible"){
            //obj.classList.remove("visible");
            //obj.classList.add("collapse");
            obj.classList.remove("in");
            console.log("btn_change_visibility : ", obj.classList);
            return;
        }   
    }
};


